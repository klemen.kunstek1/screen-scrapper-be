This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# screen-scraper-be

>  Backend REST API of project that takes an URL string and serves a screenshot of that page. Refer to [this project](https://gitlab.com/klemen.kunstek1/screen-scrapper-fe) for frontend.

## Getting Started

In the project directory, you can run:

### `npm install`

Installs dependencies.<br>

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:4000](http://localhost:3000) to view it in the browser.




