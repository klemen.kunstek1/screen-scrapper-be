var express = require("express");
var fs = require("fs");
var router = express.Router();
const puppeteer = require("puppeteer");
const base64Img = require("base64-img");

const IMAGE_PATH = "SCREENSHOT.png";

router.get("/", async function(req, res, next) {
  const url = req.query.url;
  console.log("URL IS  ===========================", url);
  try {
    await screenshot(url);
    // convert image to base64 sting
    var data = base64Img.base64Sync(IMAGE_PATH);
    res.json({ img: data });
  } catch (error) {
    res.status(400).json({ error: "BACKEND SAYS: " + error });
    console.error(error);
  }
});

/**
 * take a screenshot. PNG will be saved in project directory as "SCREENSHOT.png"
 * and will be rewritten for each request
 */
const screenshot = async url => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.setViewport({ width: 1280, height: 800 });
  await page.goto(url);
  await page.screenshot({ path: IMAGE_PATH });
  await browser.close();
};

module.exports = router;
